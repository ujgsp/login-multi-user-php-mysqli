<?php
session_start();

include "../include/header.php";

if(! isset($_SESSION['level'])){
    header("Location: ../index.html");
    exit;
}

if(isset($_SESSION['level']) && $_SESSION['level'] == "admin") {
?>
    <!-- awal menu -->
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid">
                <h1 class="mt-4">Upload</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Rekening Koran</li>
                    <li class="breadcrumb-item active">BCA</li>
                    <li class="breadcrumb-item active">BCA ETC</li>
                </ol>
                <div class="row">

                    <div class="col-xl-12">
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-database"></i>
                                Upload Rekening Koran BCA ETC
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="file" name="" id="">
                                </div> 

                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Upload</button>
                                </div> 
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </main>
    <!-- akhir menu -->
<?php
include "../include/footer.php";
}
elseif(isset($_SESSION['level']) && $_SESSION['level'] == "user") {
	echo "<script>alert('Halaman ini tidak bisa di akses!')</script>";
	echo "<script>window.location='index.php'</script>";
	// header("Location: 401.php");
}
?>
